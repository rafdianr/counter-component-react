import React, { Component } from "react";
import ButtonCompe from "../component/ButtonComp";
import Header from "../component/Header";

export class HomePage extends Component {
  constructor() {
    super();
    this.state = {
      title: "Counter",
      counter: 0,
    };
  }

  onAdd = () => {
    this.setState({
      counter: this.state.counter + 1,
    });
  };

  onSub = () => {
    this.setState({
      counter: this.state.counter - 1,
    });
  };

  render() {
    const desc = "Props description dari variable";
    return (
      <div className="homePage" style={{ textAlign: "center" }}>
        <Header
          title={this.state.title}
          desc={desc}
          name="Props name langsung inline"
        >
          <div>Children 1</div>
          <div>Children 2</div>
          <div>Children 3</div>
        </Header>
        <h3>{this.state.counter}</h3>
        <ButtonCompe textBtn="+" onClick={this.onAdd} />
        <ButtonCompe textBtn="-" onClick={this.onSub} />
      </div>
    );
  }
}

export default HomePage;
