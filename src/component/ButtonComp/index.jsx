import React from "react";
import "../../assets/styles/ButtonComp.css";

const ButtonComp = ({ textBtn, onClick }) => {
  // const handleClick = () => {
  //   onClick();
  // };
  return (
    <button className="btn" onClick={() => onClick()}>
      {textBtn}
    </button>
  );
};

export default ButtonComp;
